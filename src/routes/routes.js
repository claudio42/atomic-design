import { Switch, Route } from "react-router-dom";
import Footer from "../components/Organisms/Footer";
import Header from "../components/Organisms/Header";
import Home from "../Pages/Home";
import Xbox from "../Pages/Xbox";

const Routes = () => {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/xbox">
          <Xbox />
        </Route>
      </Switch>
      <Footer />
    </>
  );
};

export default Routes;
