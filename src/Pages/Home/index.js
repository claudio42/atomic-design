import Main from "../../components/Organisms/Main";
import Header from "../../components/Organisms/Header";
import Footer from "../../components/Organisms/Footer";

const Home = () => {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  );
};

export default Home;
