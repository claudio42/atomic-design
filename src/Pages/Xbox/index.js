import Main from "../../components/Organisms/Main";

const Home = () => {
  return (
    <>
      <Main />
    </>
  );
};

export default Home;
