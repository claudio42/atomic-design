import styled from "styled-components";

export const StyledImage = styled.img`
  width: ${(props) => props.width || "180px"};
  height: ${(props) => props.height || "180px"};
  border: none;
`;
