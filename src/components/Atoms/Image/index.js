import { StyledImage } from "./style";

const Image = ({ src, alt, width, img, height }) => {
  return (
    <StyledImage
      src={src || img}
      alt={alt || "nothing here"}
      width={width}
      height={height}
    />
  );
};

export default Image;
