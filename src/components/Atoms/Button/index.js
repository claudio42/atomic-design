import Teste from "./style";

const Button = ({ text, onClick }) => {
  return (
    <div>
      <Teste onClick={onClick}>{text}</Teste>
    </div>
  );
};

export default Button;
