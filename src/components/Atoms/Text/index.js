import StyledText from "./style";

const Text = ({ fontSize, text }) => {
  return <StyledText font-size={fontSize}>{text}</StyledText>;
};

export default Text;
