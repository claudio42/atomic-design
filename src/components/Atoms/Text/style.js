import styled from "styled-components";

const StyledText = styled.div`
  font-size: ${(props) => props.fontSize || "14px"};
  width: 300px;
`;
export default StyledText;
