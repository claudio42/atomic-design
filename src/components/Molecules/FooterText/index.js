import { Container } from "./style";
import Text from "../../Atoms/Text";

const FooterText = () => {
  return (
    <Container>
      <Text />
    </Container>
  );
};

export default FooterText;
