import { Logo } from "./style";
import Image from "../../Atoms/Image";

import img from "../../../Images/1logo.jpg";

const MainLogo = () => {
  return (
    <>
      <Logo>
        <Image src={img} />
      </Logo>
    </>
  );
};

export default MainLogo;
