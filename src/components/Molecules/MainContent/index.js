import { Container } from "./style";

import Image from "../../Atoms/Image";
import Text from "../../Atoms/Text";
import Button from "../../Atoms/Button";

const Info = ({ text, src, alt, buttonName, onClick }) => {
  return (
    <Container>
      <Image src={src} alt={alt} />
      <Text text={text} />
      <Button text={buttonName} onClick={onClick} />
    </Container>
  );
};
export default Info;
