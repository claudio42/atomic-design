import { StyledContainer } from "./style";
import Text from "../../Atoms/Text";

const insta = "instagram: @dio.gif";

const Footer = () => {
  return (
    <StyledContainer>
      <Text text={insta} />
    </StyledContainer>
  );
};
export default Footer;
