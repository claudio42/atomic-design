import { StyledContainer } from "./style";
import MainLogo from "../../Molecules/Logo";

const Header = () => {
  return (
    <StyledContainer>
      <MainLogo />
    </StyledContainer>
  );
};

export default Header;
