import { useLocation, useHistory } from "react-router-dom";
import { StyledMain } from "./style";
import Info from "../../Molecules/MainContent";

import ps5 from "../../../Images/2ps5.png";
import xbox from "../../../Images/3xbox.png";

const Main = () => {
  const history = useHistory();
  const location = useLocation();
  const textops5 =
    "The PlayStation 5 (PS5) is a home video game console developed by Sony Interactive Entertainment. Announced in 2019 as the successor to the PlayStation 4, the PS5 was released on November 12, 2020 in Australia, Japan, New Zealand, North America, Singapore and South Korea, and November 19, 2020 onwards in other major markets except China. The console is scheduled to be launched in India on February 2, 2021.";
  const textoxbox =
    "The Xbox Series X and the Xbox Series S (collectively, the Xbox Series X/S[b]) are home video game consoles developed by Microsoft. They were both released on November 10, 2020 as the fourth generation of the Xbox console family, succeeding the Xbox One family.[3]";

  return (
    <>
      {location.pathname === "/" && (
        <StyledMain>
          <Info
            text={textops5}
            src={ps5}
            alt={"Ps5"}
            buttonName="Xbox"
            onClick={() => history.push("/xbox")}
          />
        </StyledMain>
      )}
      {location.pathname === "/xbox" && (
        <StyledMain>
          <Info
            text={textoxbox}
            src={xbox}
            alt={"Xbox"}
            buttonName="Ps5"
            onClick={() => history.push("/")}
          />
        </StyledMain>
      )}
    </>
  );
};
export default Main;
