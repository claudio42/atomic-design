import styled from "styled-components";

export const StyledMain = styled.div`
  width: 100vw;
  max-width: 100%;
  height: 70vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: white;
  color: Black;
`;
